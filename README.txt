Charts Field Formatter
======

The Charts field formatter module provides a way to build any kind of chart inside an entity (e.g. node, paragraph ..etc) with Highcharts
chart provider.

As any chart data depends on tabular data, this module depends on Table field module which will let you import your chart data using a CSV file or manual editing.
